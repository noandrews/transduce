package edu.jhu.hlt.phylo.distrib; 

import java.util.ListIterator;
import java.util.List;
import java.util.ArrayList;   // for main() test method

import edu.jhu.hlt.phylo.math.Functions;

/**
 * Collection of static methods for sampling from multinomials stored
 * in arrays or iterators.  This is annoying repetitive code that
 * shows up in a lot of classes in this package.
 * <p>
 * Since Java doesn't make it easy to factor out the commonalities in
 * this code (at least not efficiently), it's good to at least put it
 * here in one place.
 */ 

public class IndexSampler {

    // ****** log double [] versions ******

    public final static int logSampleIndex(double[] a, double total) {
        double threshold = Math.log(RandomNumberGenerator.nextDouble()) + total;  // in the range [0,total)
        double partialsum = Double.NEGATIVE_INFINITY;   // once partialsum strictly exceeds threshold, we're done
        
        int i = 0;
        do {
            partialsum = Functions.log_add(partialsum,a[i++]);
        } while (partialsum <= threshold && i < a.length);

        // the i < a.length check is necessary only because of floating-point worries.
        // In principle, as long as partialsum <= threshold then partialsum < total
        // so we can't have added all elements yet and i < a.length still.
        
        return i-1;
    }
    
    public final static int logSampleIndex(double [] a) {
        double total = Double.NEGATIVE_INFINITY;
        for (double u : a) {
            total = Functions.log_add(total,u);
        }
        //        System.err.println("total = " + total);
        //        System.err.println("exp(total) = " + Math.exp(total));
        return logSampleIndex(a, total);
    }

  // ****** float[] versions ******

  /** Given a multinomial specified by an float[] array of unnormalized probabilities,
   * return the index of a random event. 
   */
  public final static int sampleIndex(float[] a) {
    float total = 0;
    for (float u : a) total += u;
    return sampleIndex(a, total);
  }

  /** Like {@link #sampleIndex(float[])}, but where the total of the array
   * is already known.
   */
  public final static int sampleIndex(float[] a, float total) {
    if (total <= 0) throw new NegativeCountException("Trying to sample from unnormalized distribution with total "+total+" which is <= 0");
    float threshold = RandomNumberGenerator.nextFloat() * total;  // in the range [0,total)
    float partialsum = 0;   // once partialsum strictly exceeds threshold, we're done

    int i = 0;
    do {
      partialsum += a[i++];
    } while (partialsum <= threshold && i < a.length);
    // the i < a.length check is necessary only because of floating-point worries.  
    // In principle, as long as partialsum <= threshold then partialsum < total 
    // so we can't have added all elements yet and i < a.length still.

    return i-1;
  }

  // ****** Float[] versions ******

  /** Given a multinomial specified by an Float[] array of unnormalized probabilities,
   * return the index of a random event.  null values are treated as 0.
   */
  public final static int sampleIndex(Float[] a) {
    float total = 0;
    for (Float u : a) if (u != null) total += u;
    return sampleIndex(a, total);
  }

  /** Like {@link #sampleIndex(Float[])}, but where the total of the array
   * is already known.
   */
  public final static int sampleIndex(Float[] a, float total) {
    if (total <= 0) throw new NegativeCountException("Trying to sample from unnormalized distribution with total "+total+" which is <= 0");
    float threshold = RandomNumberGenerator.nextFloat() * total;  // in the range [0,total)
    float partialsum = 0;   // once partialsum strictly exceeds threshold, we're done

    int i = 0;
    do {
      Float u = a[i++];
      if (u != null) partialsum += u;
    } while (partialsum <= threshold && i < a.length);
    // the i < a.length check is necessary only because of floating-point worries.  
    // In principle, as long as partialsum <= threshold then partialsum < total 
    // so we can't have added all elements yet and i < a.length still.

    return i-1;
  }

  // ****** ListIterator<Float> version ******

  /** Given a ListIterator<Float> over a list of unnormalized event probabilities,
   * sample a random event by leaving the iterator just after one of those
   * unnormalized event probabilities.  The caller can then call methods on the
   * iterator such as {@link ListIterator#set}, {@link ListIterator#remove}, or
   * {@link ListIterator#previousIndex}. 
   *
   * @param iter The iterator, which should be at the start of the list.
   *
   * @param total The sum of all unnormalized event probabilities in the list.
   *
   * @returns The unnormalized probability of the sampled event.
   */
  public final static float sampleIter(ListIterator<Float> iter, float total) {
    if (total <= 0) throw new NegativeCountException("Trying to sample from unnormalized distribution with total "+total+" which is <= 0");
    if (iter.hasPrevious()) throw new IllegalStateException("The supplied ListIterator is not at the start of the list");

    float threshold = RandomNumberGenerator.nextFloat() * total;  // in the range [0,total)
    float partialsum = 0;   // once partialsum strictly exceeds threshold, we're done

    Float u;
    do {
      u = iter.next();
      if (u != null) partialsum += u;
    } while (partialsum <= threshold && iter.hasNext());
    // the iter.hasNext() check is necessary only because of floating-point worries.  
    // In principle, as long as partialsum <= threshold then partialsum < total 
    // so we can't have added all elements yet.

    return u;
  }

  // ****** double[] versions *****
    
    // Given an array of unnormalized probabilities, sample an index at random */
    public final static int sampleIndex(double[] a) {
        double total = 0.0;
        for(double u : a) total += u;
        return sampleIndex(a,total);
    }

    public final static int sampleIndex(double[] a, double total) {
        if (total <= 0) throw new NegativeCountException("Trying to sample from a set of individuals of size "+total+" which is <= 0");
        double threshold = RandomNumberGenerator.nextDouble()*total;  // in the range [0,total)
        double partialsum = 0.0;   // once partialsum strictly exceeds threshold, we're done
        
        int i = 0;
        do {
            partialsum += a[i++];
        } while (partialsum <= threshold);
        
        return i-1;
    }

    // Given an array of unnormalized probabilities, sample an index at random */
    public final static int sampleIndex(Double[] a) {
        double total = 0.0;
        for(double u : a) total += u;
        return sampleIndex(a,total);
    }

    public final static int sampleIndex(Double[] a, double total) {
        if (total <= 0) throw new NegativeCountException("Trying to sample from a set of individuals of size "+total+" which is <= 0");
        double threshold = RandomNumberGenerator.nextDouble()*total;  // in the range [0,total)
        double partialsum = 0.0;   // once partialsum strictly exceeds threshold, we're done
        
        int i = 0;
        do {
            partialsum += a[i++];
        } while (partialsum <= threshold);
        
        return i-1;
    }

    // Given an array of unnormalized probabilities, sample an index at random */
    public final static int sampleIndex(List<Double> a) {
        double total = 0.0;
        for(double u : a) total += u;
        return sampleIndex(a,total);
    }

    public final static int sampleIndex(List<Double> a, double total) {
        if (total <= 0) throw new NegativeCountException("Trying to sample from a set of individuals of size "+total+" which is <= 0");
        double threshold = RandomNumberGenerator.nextDouble()*total;  // in the range [0,total)
        double partialsum = 0.0;   // once partialsum strictly exceeds threshold, we're done
        
        int i = 0;
        do {
            partialsum += a.get(i++);
        } while (partialsum <= threshold);
        
        return i-1;
    }

  // ****** int[] versions ******

  /** Given a multinomial specified by an int[] array of unnormalized probabilities,
   * return the index of a random event. 
   */
  public final static int sampleIndex(int[] a) {
    int total = 0;
    for (int u : a) total += u;
    return sampleIndex(a, total);
  }

  /** Like {@link #sampleIndex(int[])}, but where the total of the array
   * is already known.
   */
  public final static int sampleIndex(int[] a, int total) {
    if (total <= 0) throw new NegativeCountException("Trying to sample from a set of individuals of size "+total+" which is <= 0");
    int threshold = RandomNumberGenerator.nextInt(total);  // in the range [0,total)
    int partialsum = 0;   // once partialsum strictly exceeds threshold, we're done

    int i = 0;
    do {
      partialsum += a[i++];
    } while (partialsum <= threshold);

    return i-1;
  }

  // ****** Integer[] versions ******

  /** Given a multinomial specified by an Integer[] array of unnormalized probabilities,
   * return the index of a random event.  null values are treated as 0.
   */
  public final static int sampleIndex(Integer[] a) {
    int total = 0;
    for (Integer u : a) if (u != null) total += u;
    return sampleIndex(a, total);
  }

  /** Like {@link #sampleIndex(Integer[])}, but where the total of the array
   * is already known.
   */
  public final static int sampleIndex(Integer[] a, int total) {
    if (total <= 0) throw new NegativeCountException("Trying to sample from a set of individuals of size "+total+" which is <= 0");
    int threshold = RandomNumberGenerator.nextInt(total);  // in the range [0,total)
    int partialsum = 0;   // once partialsum strictly exceeds threshold, we're done

    int i = 0;
    do {
      Integer u = a[i++];
      if (u != null) partialsum += u;
    } while (partialsum <= threshold);

    return i-1;
  }

  // ****** ListIterator<Integer> version ******

  /** Given a ListIterator<Integer> over a list of unnormalized event probabilities,
   * sample a random event by leaving the iterator just after one of those
   * unnormalized event probabilities.  The caller can then call methods on the
   * iterator such as {@link ListIterator#set}, {@link ListIterator#remove}, or
   * {@link ListIterator#previousIndex}. 
   *
   * @param iter The iterator, which should be at the start of the list.
   *
   * @param total The sum of all unnormalized event probabilities in the list.
   *
   * @returns The unnormalized probability of the sampled event.
   */
  public final static int sampleIter(ListIterator<Integer> iter, int total) {
    if (total <= 0) throw new NegativeCountException("Trying to sample from a set of individuals of size "+total+" which is <= 0");
    if (iter.hasPrevious()) throw new IllegalStateException("The supplied ListIterator is not at the start of the list");

    int threshold = RandomNumberGenerator.nextInt(total);  // in the range [0,total)
    int partialsum = 0;   // once partialsum strictly exceeds threshold, we're done

    Integer u;
    do {
      u = iter.next();
      if (u != null) partialsum += u;
    } while (partialsum <= threshold);

    return u;
  }

  // ****** Test method ******
  public static void main(String[] args) {
    int[] u = {40,10,20,50};  // unnormalized probabilities for all tests; for each test, we'll be printing out sampling approximations to these
    int K = u.length;         // size of sample space
    int N = 1000000;          // number of samples to take

    int total=0; for (int j=0; j<K; ++j) { total += u[j]; }

    float[] f1 = new float[K];  for (int j=0; j<K; ++j) { f1[j]=u[j]; }
    Float[] f2 = new Float[K];  for (int j=0; j<K; ++j) { f2[j]=(float)u[j]; }
    ArrayList<Float> f3 = new ArrayList<Float>(3); for (int j=0; j<K; ++j) { f3.add((float)u[j]); }

    double[] d1 = new double[K]; for (int j=0; j<K; ++j) { d1[j]=(double)u[j]; }

    int[]     i1 = new int[K];  for (int j=0; j<K; ++j) { i1[j]=u[j]; }
    Integer[] i2 = new Integer[K];  for (int j=0; j<K; ++j) { i2[j]=u[j]; }
    ArrayList<Integer> i3 = new ArrayList<Integer>(3); for (int j=0; j<K; ++j) { i3.add(u[j]); }

    int[] counts;

    counts = new int[K]; for (int i=0; i<N; ++i) { ++counts[sampleIndex(f1)];        } for (int j=0; j<K; ++j) { System.out.print((total*counts[j]/(float)N)+"\t"); } System.out.println("f1");
    counts = new int[K]; for (int i=0; i<N; ++i) { ++counts[sampleIndex(f1, total)]; } for (int j=0; j<K; ++j) { System.out.print((total*counts[j]/(float)N)+"\t"); } System.out.println("f1");

    counts = new int[K]; for (int i=0; i<N; ++i) { ++counts[sampleIndex(f2)];        } for (int j=0; j<K; ++j) { System.out.print((total*counts[j]/(float)N)+"\t"); } System.out.println("f2");
    counts = new int[K]; for (int i=0; i<N; ++i) { ++counts[sampleIndex(f2, total)]; } for (int j=0; j<K; ++j) { System.out.print((total*counts[j]/(float)N)+"\t"); } System.out.println("f2");

    counts = new int[K]; for (int i=0; i<N; ++i) { ++counts[sampleIndex(d1)];        } for (int j=0; j<K; ++j) { System.out.print((total*counts[j]/(double)N)+"\t"); } System.out.println("d1");
    counts = new int[K]; for (int i=0; i<N; ++i) { ++counts[sampleIndex(d1, total)]; } for (int j=0; j<K; ++j) { System.out.print((total*counts[j]/(double)N)+"\t"); } System.out.println("d1");

    counts = new int[K]; for (int i=0; i<N; ++i) { ListIterator<Float>iter = f3.listIterator();
                                                   float uval = sampleIter(iter, total);
						   int j = iter.previousIndex();
						   if (uval != u[j]) throw new RuntimeException();  // check return value
						   ++counts[j];                      } for (int j=0; j<K; ++j) { System.out.print((total*counts[j]/(float)N)+"\t"); } System.out.println("f3");


    counts = new int[K]; for (int i=0; i<N; ++i) { ++counts[sampleIndex(i1)];        } for (int j=0; j<K; ++j) { System.out.print((total*counts[j]/(float)N)+"\t"); } System.out.println("i1");
    counts = new int[K]; for (int i=0; i<N; ++i) { ++counts[sampleIndex(i1, total)]; } for (int j=0; j<K; ++j) { System.out.print((total*counts[j]/(float)N)+"\t"); } System.out.println("i1");

    counts = new int[K]; for (int i=0; i<N; ++i) { ++counts[sampleIndex(i2)];        } for (int j=0; j<K; ++j) { System.out.print((total*counts[j]/(float)N)+"\t"); } System.out.println("i2");
    counts = new int[K]; for (int i=0; i<N; ++i) { ++counts[sampleIndex(i2, total)]; } for (int j=0; j<K; ++j) { System.out.print((total*counts[j]/(float)N)+"\t"); } System.out.println("i2");

    counts = new int[K]; for (int i=0; i<N; ++i) { ListIterator<Integer>iter = i3.listIterator();
                                                   int uval = sampleIter(iter, total);
						   int j = iter.previousIndex();
						   if (uval != u[j]) throw new RuntimeException();  // check return value
						   ++counts[j];                      } for (int j=0; j<K; ++j) { System.out.print((total*counts[j]/(float)N)+"\t"); } System.out.println("i3");

    // Log sampling
    double [] log_d1 = new double[K]; for(int j=0;j<K;++j) { log_d1[j] = Math.log(u[j]); };
    counts = new int[K]; for (int i=0; i<N; ++i) { ++counts[logSampleIndex(log_d1)]; } for (int j=0; j<K; ++j) { System.out.print((total*counts[j]/(float)N)+"\t"); } System.out.println("log d1");

    double [] u2 = {0.1,0.1,0.3,0.9};
    double total2=0; for (int j=0; j<K; ++j) { total += u2[j]; }

    double [] log_d2 = new double[K]; for(int j=0;j<K;++j) { log_d2[j] = Math.log(u2[j]); };
    counts = new int[K]; for (int i=0; i<N; ++i) { ++counts[logSampleIndex(log_d2)]; } for (int j=0; j<K; ++j) { System.out.print(counts[j]/(double)N+"\t"); } System.out.println("log d2");

  }
}
