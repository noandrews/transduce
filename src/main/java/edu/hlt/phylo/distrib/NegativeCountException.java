package edu.jhu.hlt.phylo.distrib;

/**
 * This exception is thrown when we attempt to unobserve more events
 * than we have observed (see {@link Distribution#observe}), or when
 * an unnormalized distribution sums to <= 0.
 */

class NegativeCountException extends RuntimeException {
  NegativeCountException(String message) {
    super(message);
  }
}