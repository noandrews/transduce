package edu.jhu.hlt.phylo.distrib;

import edu.jhu.hlt.phylo.util.MersenneTwisterFast; // This is strictly better than java.util.Random.
                                     // There exist faster generators, but at the expense
                                     // of shorter periods.

import org.apache.commons.math3.random.Well44497b;
import org.apache.commons.math3.random.RandomGenerator;

/**
 * A wrapper class for a static random number generator.
 *
 * @author Nicholas Andrews
 */
// TODO: We could have several versions for different underlying rngs,
//       but I don't see a need.  Also, you can't do "static abstract"
//       in Java, so they'd each have to be their own stand-alone
//       class.
public class RandomNumberGenerator {
    static RandomGenerator rng = new Well44497b(0);
    static public void setSeed(int seed) {
        rng.setSeed(seed);
    }
    static public double nextDouble() {
        return rng.nextDouble();
    }
    static public float nextFloat() {
        return rng.nextFloat();
    }
    static public int nextInt(int total) {
        return rng.nextInt(total);
    }
    static public double nextGaussian() {
        return rng.nextGaussian();
    }
    static public RandomGenerator getGenerator() {
        return rng;
    }
}