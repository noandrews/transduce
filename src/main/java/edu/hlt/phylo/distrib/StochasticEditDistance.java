/**
 * Implementation based on "Learning String Edit Distance" by Ristad.
 *
 * @author Nicholas Andrews
 */

package edu.jhu.hlt.phylo.distrib;

import java.util.List;
import java.util.StringTokenizer;

import com.google.common.collect.*;

import cc.mallet.types.Alphabet;

public class StochasticEditDistance implements StringTransducer {

	final static double MIN_DELTA = 0.01; // EM convergence threshold

	final static int SUBST = 0;
	final static int INSER = 1;
	final static int DELET = 2;
	final static int STOP  = 3; // only this is actually used

	// Probabilities
	double [][] subst_prob;
	double []   inser_prob;
	double []   delet_prob;
	double []      op_prob;

	// Expectations
	double [][]  subst_exp;
	double []    inser_exp;
	double []    delet_exp;
	double []       op_exp;

	public StochasticEditDistance(int ntypes) {
		subst_prob = new double[ntypes][ntypes]; subst_exp  = new double[ntypes][ntypes];
		inser_prob = new double[ntypes];         inser_exp  = new double[ntypes];
		delet_prob = new double[ntypes];         delet_exp  = new double[ntypes];
		op_prob    = new double[4];                 op_exp  = new double[4];

		random_init(); // set some initial probabilities randomly
	}

	// EM with random restarts
	public void train(int [][] xs, int [][] ys, int nrestart) {
		System.err.println("[StochasticEditDistance] Train called without weights; assuming uniform");
		double [] weights = new double[xs.length];
		for(int i=0;i<xs.length;i++) weights[i] = 1.0;
		train(xs,ys,weights,nrestart);
	}
	public void train(int [][] xs, int [][] ys, double [] weights, int nrestart) {
		assert(xs.length == ys.length);
		assert(ys.length == weights.length);
		assert(nrestart > 0);

		double bestll = Double.NEGATIVE_INFINITY;
		double currll;

		double [][] tmp_subst_prob = new double[delet_prob.length][delet_prob.length];
		double []   tmp_inser_prob = new double[delet_prob.length];
		double []   tmp_delet_prob = new double[delet_prob.length];
		double []      tmp_op_prob = new double[4];

		for(int n=0;n<nrestart;n++) {
			random_init();
			currll = em(xs,ys,weights);
			if(currll > bestll) {
				copy_probs(true,tmp_subst_prob,tmp_inser_prob,tmp_delet_prob,tmp_op_prob);
				bestll = currll;
			}
		}

		System.err.println("[StochasticEditDistance] Best LL = " + bestll);

		// Set the best params
		copy_probs(false,tmp_subst_prob,tmp_inser_prob,tmp_delet_prob,tmp_op_prob);
	}

    public void train(int [][] train_xs, int [][] train_ys, double [] train_weights,
	       int [][] test_xs, int [][] test_ys, double [] test_weights,
	       int nrestart) {
	System.err.println("FIXME: Ignoring test examples");
	train(train_xs, train_ys, train_weights, nrestart);
    }
    
    public void train(int [][] train_xs, int [][] train_ys, int [][] test_xs, int [][] test_ys, int nrestart) {
	System.err.println("FIXME: Ignoring test examples");
	train(train_xs, train_ys, nrestart);
    }

	// Returns the log likelihood
	public double em_step(int [][] xs, int [][] ys, double [] weights) {
		zero_expectations();
		double ll = 0.0;
		for(int i=0;i<xs.length;i++) {
			ll += Math.log(expectation_step(xs[i],ys[i],weights[i]));
		}
		maximization_step();
		sanity_check();       // XXX
		return ll;
	}

	// Evaluate the probability of the given input and output
	public double p(int [] x, int [] y) {
		double [][] alphas = forward_evaluate(x,y);
		return alphas[x.length][y.length];
	}

    public double logp(int [] x, int [] y) {
        double [][] alphas = forward_evaluate(x,y);
		return Math.log(alphas[x.length][y.length]); // fixme
     }

	// Calculate the probability p(x,y|\theta) 
	// O(T*V) time and space where |x| = T and |y| = V
	private double [][] forward_evaluate(int [] x, int [] y) {
		//	System.err.println("Forward eval:");
		double [][] alpha = new double[x.length+1][y.length+1];
		int [] a = new int[x.length+1]; for(int i=0,t=1;i<x.length;i++,t++) a[t] = x[i];
		int [] b = new int[y.length+1]; for(int i=0,t=1;i<y.length;i++,t++) b[t] = y[i];
		alpha[0][0] = 1.0;
		for(int t=0;t<=x.length;t++) {
			for(int v=0;v<=y.length;v++) {
				if(v>0 || t>0) alpha[t][v]  = 0.0;
				if(v>0)        alpha[t][v] += inser_prob[b[v]]*alpha[t][v-1];           // insertion
				if(t>0)        alpha[t][v] += delet_prob[a[t]]*alpha[t-1][v];           // deletion
				if(v>0 && t>0) alpha[t][v] += subst_prob[a[t]][b[v]]*alpha[t-1][v-1];   // substitution
				//		System.err.print(alpha[t][v] + " ");
			}
			//	    System.err.println("");
		}
		alpha[x.length][y.length] *= op_prob[STOP];
		return alpha;
	}

	public void sanity_check() {
		for(int i=0;i<subst_prob.length;i++) {
			double sum = 0.0;
			for(int j=0;j<subst_prob[0].length;j++) {
				sum += subst_prob[i][j];
			}
			assert(sum > 1.0-1E-4 && sum < 1.0+1E-4);
		}
	}

	private double [][] backward_evaluate(int [] x, int [] y) {
		//	System.err.println("Backward eval:");
		double [][] beta = new double[x.length+1][y.length+1];
		int [] a = new int[x.length+1]; for(int i=0,t=1;i<x.length;i++,t++) a[t] = x[i];
		int [] b = new int[y.length+1]; for(int i=0,t=1;i<y.length;i++,t++) b[t] = y[i];
		beta[x.length][y.length] = op_prob[STOP];
		for(int t=x.length; t>=0; t--) {
			for(int v=y.length; v>=0; v--) {
				if(t<x.length || v<y.length) beta[t][v]  = 0.0;
				if(v<y.length)               beta[t][v] += inser_prob[b[v+1]]*beta[t][v+1];
				if(t<x.length)               beta[t][v] += delet_prob[a[t+1]]*beta[t+1][v];
				if(t<x.length && v<y.length) beta[t][v] += subst_prob[a[t+1]][b[v+1]]*beta[t+1][v+1];
				//		System.err.print(beta[t][v] + " ");
			}
			//	    System.err.println("");
		}
		return beta;
	}

	// Guess some expectations, then do an M-step to get probabilities
	private void random_init() {
		op_exp[STOP] = RandomNumberGenerator.nextDouble();
		for(int i=0;i<inser_prob.length;i++) {
			inser_exp[i] = RandomNumberGenerator.nextDouble();
		}
		for(int i=0;i<delet_prob.length;i++) {
			delet_exp[i] = RandomNumberGenerator.nextDouble();
		}
		for(int i=0;i<subst_prob.length;i++) {
			for(int j=0;j<subst_prob[0].length;j++) {
				subst_exp[i][j] = RandomNumberGenerator.nextDouble();
			}
		}
		maximization_step();
	}

	private double em(int [][] x, int [][] y, double [] weights) {
		boolean converged = false;
		double ll = Double.NEGATIVE_INFINITY, prevll = Double.NEGATIVE_INFINITY;
		int iter = 0;
		while(!converged) {
			zero_expectations();
			ll = 0.0;
			for(int i=0;i<x.length;i++) {
				if(weights[i] > 0.0) {
					ll += Math.log(expectation_step(x[i],y[i],weights[i]));
				}
			}
			//System.err.println("iter "+iter+" ll="+ll);
			maximization_step();
			if(Math.abs(ll-prevll) < MIN_DELTA) converged = true;
			prevll = ll;
			iter++;
		}
		//System.err.println("EM converged after " + iter + " iterations");
		return ll;
	}

	private double expectation_step(int [] x, int [] y, double weight) {
		double [][] alpha = forward_evaluate(x,y);
		double [][] beta  = backward_evaluate(x,y);
		double dist = alpha[x.length][y.length];
		//	System.err.println("forward prob = "  + alpha[x.length][y.length]);
		//	System.err.println("backward prob = " + beta[0][0]);
		assert(Math.abs(alpha[x.length][y.length] - beta[0][0]) < 1E-5);
		if(dist == 0.0) return 0.0;
		double invZ = 1.0 / alpha[x.length][y.length];
		op_exp[STOP] += weight;
		int [] a = new int[x.length+1]; for(int i=0,t=1;i<x.length;i++,t++) a[t] = x[i];
		int [] b = new int[y.length+1]; for(int i=0,t=1;i<y.length;i++,t++) b[t] = y[i];
		for(int t=0;t<=x.length;t++) {
			for(int v=0;v<=y.length;v++) {
				if(t>0)        delet_exp[a[t]] += alpha[t-1][v]*delet_prob[a[t]]*beta[t][v]*invZ*weight;
				if(v>0)        inser_exp[b[v]] += alpha[t][v-1]*inser_prob[b[v]]*beta[t][v]*invZ*weight;
				if(t>0 && v>0) subst_exp[a[t]][b[v]] += alpha[t-1][v-1]*subst_prob[a[t]][b[v]]*beta[t][v]*invZ*weight;
			}
		}
		return dist;
	}

	private void maximization_step() {

		// Calculate the normalizing constant
		double N = op_exp[STOP];
		for(int i=0;i<inser_prob.length;i++) {
			N += inser_exp[i];
		}
		for(int i=0;i<delet_prob.length;i++) {
			N += delet_exp[i];
		}
		for(int i=0;i<subst_prob.length;i++) {
			for(int j=0;j<subst_prob[0].length;j++) {
				N += subst_exp[i][j];
			}
		}

		// Normalize to get probabilities
		double invN = 1.0 / N;
		for(int i=0;i<inser_prob.length;i++) {
			inser_prob[i] = inser_exp[i] * invN;
		}
		for(int i=0;i<delet_prob.length;i++) {
			delet_prob[i] = delet_exp[i] * invN;
		}
		for(int i=0;i<subst_prob.length;i++) {
			for(int j=0;j<subst_prob[0].length;j++) {
				subst_prob[i][j] = subst_exp[i][j] * invN;
			}
		}
		op_prob[STOP] = op_exp[STOP] / N;

	}

	public void print_probs() {
		System.err.println("Insert probs: ");
		for(int i=0;i<inser_prob.length;i++) {
			System.err.print(inser_prob[i] + " ");
		}
		System.err.println("\nDelete probs: ");
		for(int i=0;i<delet_prob.length;i++) {
			System.err.print(delet_prob[i] + " ");
		}
		System.err.println("\nSubst probs: ");
		for(int i=0;i<subst_prob.length;i++) {
			for(int j=0;j<subst_prob[0].length;j++) {
				System.err.print(subst_prob[i][j] + " ");
			}
			System.err.println("");
		}
		System.err.println("Stop prob: " + op_prob[STOP]);
	}

	private void copy_probs(boolean to_tmp,
			double [][] tmp_subst_prob,
			double   [] tmp_inser_prob,
			double   [] tmp_delet_prob,
			double   [] tmp_op_prob) {
		for(int i=0;i<inser_prob.length;i++) {
			if(to_tmp) tmp_inser_prob[i] = inser_prob[i];
			else       inser_prob[i] = tmp_inser_prob[i];
		}
		for(int i=0;i<delet_prob.length;i++) {
			if(to_tmp) tmp_delet_prob[i] = delet_prob[i];
			else       delet_prob[i] = tmp_delet_prob[i];
		}
		for(int i=0;i<subst_prob.length;i++) {
			for(int j=0;j<subst_prob[0].length;j++) {
				if(to_tmp) tmp_subst_prob[i][j] = subst_prob[i][j];
				else       subst_prob[i][j] = tmp_subst_prob[i][j];
			}
		}
		if(to_tmp) tmp_op_prob[STOP] = op_prob[STOP];
		else       op_prob[STOP] = tmp_op_prob[STOP];
	}

	private void zero_expectations() {
		for(int i=0;i<inser_prob.length;i++) {
			inser_exp[i] = 0.0;
		}
		for(int i=0;i<delet_prob.length;i++) {
			delet_exp[i] = 0.0;
		}
		for(int i=0;i<subst_prob.length;i++) {
			for(int j=0;j<subst_prob[0].length;j++) {
				subst_exp[i][j] = 0.0;
			}
		}
		op_exp[STOP] = 0.0;
	}

	public static int [] str2arr(String x, Alphabet a) {
		int [] res = new int[x.length()];
		for(int i=0;i<x.length();i++) {
			int index = a.lookupIndex(x.charAt(i));
			if(index<0) {
				System.err.println(x.charAt(i));
			}
			assert(index>=0);
			res[i] = index;
		}
		return res;
	}

	public static String arr2str(int [] x, Alphabet a) {
		char [] str = new char[x.length];
		for(int i=0;i<x.length;i++) {
			str[i] = (Character)a.lookupObject(x[i]); // TODO: test this
		}
		return new String(str);
	}

	public static int [] wrd2arr(String x, Alphabet a) {
		String [] tokens = x.split("\\s+");
		int [] res = new int[tokens.length];
		for(int i=0;i<tokens.length;i++) {
			int index = a.lookupIndex(tokens[i]);
			if(index<0) {
				System.err.println(tokens[i]);
			}
			assert(index>=0);
			res[i] = index;
		}
		return res;
	}

	public static String arr2wrd(int [] x, Alphabet a) {
		StringBuilder builder = new StringBuilder();
		for(int i=0;i<x.length;i++) {
			builder.append((String)a.lookupObject(x[i]));
		}
		return builder.toString();
	}

	public static String arr_str_repr(int [] x) {
		StringBuilder builder = new StringBuilder();
		for(int i=0;i<x.length;i++) {
			builder.append(x[i] + " ");
		}
		return builder.toString();
	}

	public static void main(String args[]) {

		// Trivial
		int [][] xs0 = new int[1][];
		int [][] ys0 = new int[1][];
		xs0[0] = new int[3]; xs0[0][0] = 0; xs0[0][1] = 1; xs0[0][2] = 1;
		ys0[0] = new int[2]; ys0[0][0] = 2; ys0[0][1] = 2;
		System.err.println("Training on single instance: 011 -> 22");
		StochasticEditDistance dist0 = new StochasticEditDistance(3);
		dist0.train(xs0,ys0,1);

		// Less trivial
		System.err.println("\n====================== EM WITH RESTARTS =========================\n");
		Character [] letters = {'a','b','c'};
		Alphabet types = new Alphabet(letters);
		String [] strxs = {"aaa","aaa","aaa","aaa","bbb","bbb","bbb","ccc","ccc","ccc"};
		String [] strys = {"bbb","bbb","ccc","ccc","ccc","cc" ,"c"  ,"aca","cac","aaa"};

		int [][] xs = new int[strxs.length][];
		int [][] ys = new int[strys.length][];
		for(int i=0;i<xs.length;i++) {
			xs[i] = str2arr(strxs[i],types);
			ys[i] = str2arr(strys[i],types);
		}

		System.err.println(types.size() + " types");
		StochasticEditDistance dist = new StochasticEditDistance(types.size());
		dist.train(xs,ys,10);
		dist.print_probs();

		// MAP estimation
		System.err.println("\n====================== MAP ESTIMATION ===========================\n");
		StochasticEditDistance map_dist = new StochasticEditDistance(types.size());
		map_dist.train(xs,ys,10);
		map_dist.print_probs();
	}
}

