package edu.jhu.hlt.phylo.distrib;

import edu.jhu.hlt.phylo.lang.AnnotatedString;
import edu.jhu.hlt.phylo.lang.Alphabet;

public interface StringEditModel {
    
    public void setAlphabet(Alphabet A);
    public Alphabet getAlphabet();

    public double logp(AnnotatedString input, AnnotatedString output);
    public double [] logp(AnnotatedString [] inputs, AnnotatedString [] outputs);
    public double calc_ll(AnnotatedString [] inputs, AnnotatedString [] outputs, double [] weights);
    public double calc_ll(AnnotatedString [] inputs, AnnotatedString [] outputs);

    public double em_step(AnnotatedString [] inputs, AnnotatedString [] outputs, double [] weights);

    public void train(AnnotatedString [] train_inputs, AnnotatedString [] train_outputs,
                      AnnotatedString [] test_inputs,  AnnotatedString [] test_outputs);

    public AnnotatedString sample(AnnotatedString input);
}