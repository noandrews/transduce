/**
 * An interface for string-to-string transducers.
 *
 * @author Nicholas Andrews, Johns Hopkins University
 */

package edu.jhu.hlt.phylo.distrib;

import cc.mallet.types.Alphabet;

public interface StringTransducer {
    
    void train(int [][] xs, int [][] ys, int restart);
    void train(int [][] xs, int [][] ys, double [] weights, int nrestart);
    void train(int [][] train_xs, int [][] train_ys, double [] train_weights,
	       int [][] test_xs, int [][] test_ys, double [] test_weights,
	       int nrestart);
    void train(int [][] train_xs, int [][] train_ys, int [][] test_xs, int [][] test_ys, int nrestart);
    double em_step(int [][] xs, int [][] ys, double [] weights); // returns the log prob of the corpus
    double p(int [] x, int [] y);
    double logp(int [] x, int [] y);
    //    void setAlphabets(Alphabet isyms, Alphabet osyms);
    //int [] sample_output(int [] x);

}