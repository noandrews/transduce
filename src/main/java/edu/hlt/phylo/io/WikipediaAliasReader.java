package edu.jhu.hlt.phylo.io;

import java.util.*;
import com.google.common.collect.*;
import com.google.common.base.Charsets;
import java.nio.charset.Charset;
import com.google.common.io.Files;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

import java.io.*;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;

import edu.jhu.hlt.phylo.match.SequencePruner;
//import edu.jhu.hlt.phylo.util.EntityMention;
import edu.jhu.hlt.phylo.distrib.IndexSampler;

// import org.jgrapht.UndirectedGraph;
// import org.jgrapht.graph.DefaultDirectedGraph;
// import org.jgrapht.graph.DefaultEdge;
// import org.jgrapht.graph.SimpleGraph;
// import org.jgrapht.alg.ConnectivityInspector;
// import org.jgrapht.alg.StrongConnectivityInspector;
// import org.jgrapht.alg.BellmanFordShortestPath;

// DEBUG
//import com.wcohen.ss.Jaccard;
//import com.wcohen.ss.BasicStringWrapper;
// DEBUG

public class WikipediaAliasReader {

    boolean RESTRICT_PUNCTUATION = true;
    boolean DROP_NON_LATIN_NAMES = true;
    //boolean valid = input.matches("\\p{L}+");
    Pattern latin_only    = Pattern.compile("\\p{L}+");

    List<String> entity_canonical_names = Lists.newArrayList(); // the canonical name for each entity

    Map<String,List<String>> aliases     = Maps.newHashMap();   // aliases for each canonical name
    Map<String,Set<String>> name_aliases = Maps.newHashMap();   // for each name, all of its aliases

    List<String> names = Lists.newArrayList();           // name alias strings (with duplicates)
    Set<String> unique_names = Sets.newHashSet();        // all unique name strings (including canonical names and aliases)
    List<Integer> labels = Lists.newArrayList();         // entity label from Wikipedia
    Map<String,Integer> namefreq = Maps.newHashMap();    // frequency estimated against a large corpus
    int num_entities = 0;                                // number of distinct entities (lines in input file)

    List<Set<String>> clusters = Lists.newArrayList();

    Map<String,Integer> name_labels = Maps.newHashMap();

    // Entity canonical name labels
    public List<Integer> entity_labels() {
        List<Integer> labels = new ArrayList<Integer>();
        for(int i=0;i<num_entities;i++) {
            labels.add(i);
        }
        return labels;
    }

    public int numEntities() {
        return num_entities;
    }

    public static Set<String> find_cluster(String type, List<Set<String>> clusters) {
        for(Set<String> cluster : clusters) {
            if(cluster.contains(type)) {
                return cluster;
            }
        }
        // Should never get here
        return null;
    }

    // NOTE: this returns the first matching cluster;
    //       a type may in principle be in multiple 
    //       clusters
    public Set<String> find_cluster(String type) {
        for(Set<String> cluster : clusters) {
            if(cluster.contains(type)) {
                return cluster;
            }
        }
        // Should never get here
        return null;
    }

    public List<Set<String>> get_clusters() {
        return clusters;
    }
    
    // Returns all aliases of the given name
    public Set<String> given_name_aliases(String name) {
        return name_aliases.get(name);
    }
    
    // Entity canonical names
    public List<String> entity_canonical_names() {
        return entity_canonical_names;
    }

    public int get_name_entity(int index) {
        return labels.get(index);
    }

    public int get_name_label(String name) {
        if(name_labels.containsKey(name)) {
            return name_labels.get(name);
        }
        return -1;
    }
    
    // Unique canonical names
    public Set<String> canonical_names() {
        return aliases.keySet();
    }

    // Alises for each canonical name (not respecting entity boundaries)
    public List<String> aliases(String canonical_name) {
        if(aliases.containsKey(canonical_name)) {
            return aliases.get(canonical_name);
        }
        return null;
    }

    // Return a list of all aliases (with duplicates)
    public List<String> aliases() {
        return names;
    }

    // Return a set of all canonical names and aliases
    public Set<String> unique_names() {
        return unique_names;
    }

    public List<Integer> labels() {
        return labels;
    }
    
    // The main method: 
    //   1) Read the wikipedia file
    //   2) Filter the names
    //   3) Parse the content of the file into useful data structures
    public void filter_and_parse(String filename) {
        List<String> lines = read_wikipedia_file(filename);
        lines = filter_wikipedia_file(lines);
        num_entities = lines.size();
        parse_wikipedia_file(lines);
    }
    
    // Read lines from a Wikipedia name list
    public List<String> read_wikipedia_file(String filename) {
        List<String> lines = null;
        try {
            lines = Files.readLines(new File(filename), Charsets.UTF_8);
        }  catch (IOException e) {
            System.out.println("Unable to read "+filename+": "+e.getMessage());
        }
        System.err.println(lines.size() + " lines read");
        return lines;
    }
    
    // currently a no-op
    public List<String> filter_wikipedia_file(List<String> lines) {
        return lines;
    }
    
    // Parse Wikipedia file
    public void parse_wikipedia_file(List<String> lines) {
        
        int entity = 0;

        int nskipped = 0;
        int ntotal = 0;

        for(String line : lines) {

            // Each line corresponds to an entity
            String [] raw_tokens = line.split("\t");

            // Skip any blank lines
            if(raw_tokens.length == 0) {
	            //                System.err.println("Skipping line: " + line);
	            nskipped += 1;
                continue;
            }
            ntotal += 1;

            // Filter the raw tokens
            List<String> filtered_tokens = new ArrayList<String>();
            for(String t : raw_tokens) {
                if(!t.equals("") && t.length() >= 3) {
                    // If we're filtering based on character set, do that test here
                    if(DROP_NON_LATIN_NAMES) {

                        boolean matches = true;

                        for(char c : t.toCharArray()) {
                            String str = new String();
                            str += c;
                            //                            if(str.matches("\\p{L}+") 
                            if(str.matches("^\\p{ASCII}*$")
                               || str.matches("\\s") 
                               || str.matches("\\p{Punct}")) {
                                //                                System.err.println("match: " + str);
                            } else {
                                //                                System.err.println("no match: " + str);
                                matches = false;
                            }
                        }

                        if(matches) {

                            // if(NORMALIZE_PUNCTUATION) {
                            //     for(char c : t.toCharArray()) {
                            //         String str = ""+c;
                            //         if(str.matches("\\p{Punct}")) {
                                        
                            //         }
                            //     }
                            // }

                            filtered_tokens.add(t);
                        } else {
                            //                            System.err.println("Dropping bad name: " + t);
                        }
                    }
                }
            }

            if(filtered_tokens.size() <= 1) {
	            nskipped += 1;
                //                System.err.println("Skipping line: " + line);
                continue;
            }

            String [] tokens = filtered_tokens.toArray(new String[filtered_tokens.size()]);

            // Put everything in the cluster
            Set<String> cluster = Sets.newHashSet();
            for(String t : tokens) {
                cluster.add(t);
            }
            clusters.add(cluster);

            String canonical_name = tokens[0];
            entity_canonical_names.add(canonical_name);
            name_labels.put(canonical_name, entity); // FIXME: duplicates

            if(!aliases.containsKey(canonical_name)) {
                aliases.put(canonical_name,new ArrayList<String>());
            }
            
            int index = 0;
            for(int i=1;i<tokens.length;i++) {
                names.add(tokens[i]);
                name_labels.put(tokens[i], entity); // FIXME: duplicates
                labels.add(entity);
                aliases.get(canonical_name).add(tokens[i]);
                index += 1;
            }
            entity++;
            
            // Construct the name alias set for this entity
            // NOTE: This will conflate entities
            Set<String> all_entity_names = Sets.newHashSet(tokens); // includes the "self" alias
            for(String name : tokens) {
                if(name_aliases.containsKey(name)) {
                    name_aliases.get(name).addAll(all_entity_names);
                } else {
                    name_aliases.put(name, all_entity_names);
                }
            }
            
            // Construct the set of all names (titles + aliases)
            for(String t : tokens) {
                unique_names.add(t);
            }
        }
        
        assert(names.size() == labels.size());

        System.err.println(nskipped + " skipped lines of " + ntotal);
        System.err.println(entity + " entities and " + unique_names.size() + " total distinct names");
    }
    
    public boolean match(String name1, String name2) {
        return name_aliases.get(name1).contains(name2);
    }

    public int name_freq(String name) {
        if(namefreq.containsKey(name)) {
            return namefreq.get(name);
        } 
        return 1;
    }

    public void free_name_freq_hash() {
        namefreq = null;
    }

    public void load_name_freq_file(String filename) {
        System.out.println("Loading the name frequency file: " + filename + " ...");

        // See if a serialized version of it exists
        File f = new File(filename + ".serialized");
        if(f.exists()) {
            
            try {
                System.out.print("[WikipediaAliasReader] A serialized name frequency hash exists, reading it...");
                ObjectInputStream objIn = new ObjectInputStream(new FileInputStream(filename + ".serialized"));
                namefreq = (HashMap) objIn.readObject();
                System.out.println("done!");
            } catch (IOException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
                throw new RuntimeException();
            }  catch(ClassNotFoundException c) {
                System.out.println(c.getMessage());
                c.printStackTrace();
                throw new RuntimeException();
            }

        }
        else {
            
            int total = 0;
            int lines_read = 0;
            try {
                
                BufferedReader br = new BufferedReader(new FileReader(filename));
                String line;
                while ((line = br.readLine()) != null) {
                    String [] tokens = line.split("\t");
                    int freq = Integer.parseInt(tokens[1]);
                    assert(tokens.length == 2); assert(freq>0);
                    total += freq;
                    lines_read ++;
                    namefreq.put(tokens[0],freq);
                }
               

                // Serialize the loaded hash
                System.out.print("[WikipediaAliasReader] Serializing the name frequency hash...");
                FileOutputStream out = new FileOutputStream(filename + ".serialized");
                ObjectOutputStream objOut = new ObjectOutputStream(out);
                objOut.writeObject(namefreq);
                objOut.close();
                System.out.println("done!");
                
            } catch (IOException e) {
                System.out.println(e.getMessage());
                throw new RuntimeException();
            }
            
        }

        System.out.println(namefreq.keySet().size() + " name with associated frequencies.");
    }
    
    public Map<String,Integer> get_name_distribution() {

        // The return hash
        Map<String,Integer> ret = Maps.newHashMap();

        int num_total = unique_names.size();
        int num_oov   = 0;

        for(String s : unique_names) {
            if(namefreq.containsKey(s)) {
                ret.put(s, namefreq.get(s) );
            } else {
                ret.put(s, 1);
                num_oov += 1;
            }
        }

        System.err.println("==== NAME DISTRIBUTION: " + num_oov + " / " + num_total + " = " + (double)num_oov/(double)num_total + " OOV");

        return ret;
    }

    // public void print_graph_stats() {
        
    //     // Create an undirected graph to find connected components
    //     //SimpleGraph<String, DefaultEdge> G = new SimpleGraph<String, DefaultEdge>(DefaultEdge.class);
    //     DefaultDirectedGraph<String, DefaultEdge> G = new DefaultDirectedGraph<String, DefaultEdge>(DefaultEdge.class);

    //     // Add a vertex for each distinct name
    //     for(String name : unique_names) {
    //         G.addVertex(name);
    //     }

    //     // Determine allowed edges by pruning the graph
    //     SequencePruner sp = new SequencePruner();
    //     sp.make_index(unique_names);

    //     double total = 0.0;
    //     int max = 0;

    //     for(String name : unique_names) {
    //         Collection<String> likes = sp.like(name);
    //         if(likes.size() > max) {
    //             max = likes.size();
    //         }
    //         total += likes.size();
    //         for(String s : likes) {
    //             if(!s.equals(name)) {
    //                 G.addEdge(name,s);
    //             }
    //         }
    //     }

    //     System.err.println("finding shortest path...");
    //     List<DefaultEdge> path = BellmanFordShortestPath.findPathBetween(G, "Bramante", "Katie lucas");
    //     if(path!=null) {
    //         for(DefaultEdge e : path) {
    //             System.err.println(e.toString());
    //         }
    //     } else {
    //         System.err.println("No path!");
    //     }

    //     System.err.println("...");

    //     System.err.println("finding shortest path...");
    //     List<DefaultEdge> path2 = BellmanFordShortestPath.findPathBetween(G, "Qudrat-Ullah Shahab", "Demetrius I Starshy");
    //     if(path2!=null) {
    //         for(DefaultEdge e : path2) {
    //             System.err.println(e.toString());
    //         }
    //     } else {
    //         System.err.println("No path!");
    //     }

    //     throw new RuntimeException(); // XXX

    //     Jaccard jac_sim = new Jaccard();
    //     System.err.println("Score: " + jac_sim.score(new BasicStringWrapper("J. Spaceman"), new BasicStringWrapper("Maria Walsh")));

    //     total /= (double)unique_names.size();
    //     System.out.println("Largest number of edges = " + max);
    //     System.out.println("Mean number of edges = " + total);

    //     // computes all connected components of the graph
    //     ConnectivityInspector ci = new ConnectivityInspector(G);
    //     //        StrongConnectivityInspector ci = new StrongConnectivityInspector(G);
    //     List<Set<String>> connected_subgraphs = ci.connectedSets();

    //     int nsubgraph = connected_subgraphs.size();
    //     System.out.println(nsubgraph + " connected subgraphs");

    //     total = 0.0;
    //     max = 0;
    //     // Maximum size subgraph
        
    //     int cc = 0;
    //     for(Set<String> s : connected_subgraphs) {
    //         int size = s.size();
    //         if(size > max) {
    //             max = size;
    //         }
    //         total += size;

    //         System.err.println("component " + cc++ + "("+size+")");
    //         for(String str : s) {
    //             System.err.println("\t"+str);
    //         }
    //     }

    //     System.out.println("Largest connected component = " + max);
    //     System.out.println("Mean size = " + total / nsubgraph);
    // }

    private static Options createOptions() {
        
        Options options = new Options();
        
        // Modes
        options.addOption("p","pairs",false,"Create transducer training pairs from a Wikipedia alias list.");
        options.addOption("s","stats",false,"Print graph statistics.");

        options.addOption("m","mkcorpus",true,"Create an evaluation corpus."); // arg=outfilename
        options.addOption("size","size",true,"Size of eval corpus.");
        
        // I/O
        options.addOption("f","freq",true,"Frequency file.");
        options.addOption("i","input",true,"Path to a Wikipedia alias list for training.");
        options.addOption("o","output",true,"Path to write the training pairs.");
        options.addOption("fs","fold_sizes_file",true,"Path to fold sizes file.");
        
        // Options
        options.addOption("r","include-root",false,"Include pairs from synthetic root vertex.");
        
        return options;

    }

    //                        weighted_entity_sample_without_replacement(1500);
    public Set<Integer> weighted_entity_sample_without_replacement(int n) {
        Integer [] counts = new Integer[clusters.size()]; // total token counts for each entity
        int corpus_size = 0;
        for(int i=0;i<clusters.size();i++) {
            Set<String> cluster = clusters.get(i);
            int total = 0;
            for(String s : cluster) {
                total += name_freq(s);
            }
            counts[i] = total;
            corpus_size += total;
        }
        Set<Integer> sampled_entities = new HashSet<Integer>();
        while(sampled_entities.size() < n) {
            int k = IndexSampler.sampleIndex(counts, corpus_size);
            sampled_entities.add(k);
        }
        return sampled_entities;
    }
}
