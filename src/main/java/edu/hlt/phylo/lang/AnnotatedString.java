package edu.jhu.hlt.phylo.lang;

import gnu.trove.map.hash.TCharIntHashMap;

import edu.jhu.hlt.phylo.lang.Alphabet;
import edu.jhu.hlt.phylo.lang.PhoneticDictionary.Alignment;

public class AnnotatedString {
    static Alphabet A = new Alphabet();
    static PhoneticDictionary P = new ArpabetPhoneticDictionary(A);
    static TCharIntHashMap charfreq = new TCharIntHashMap();

    public static enum Language {Arabic, English, Czech, French, Urdu, Spanish, German}
    public static enum EntityType {GPE, PER, FAC, WEA, VEH, LOC, ORG}

    Language language;
    EntityType type;

    String s;
    Alignment a;

    public AnnotatedString(String s) {
        this(s, Language.English, EntityType.PER);
    }

    public AnnotatedString(String s, Language l, EntityType t) {
        this.s = s;
        this.language = l;
        this.type = t;
        
        if(P.initialized()) {
            a = P.getAlignment(s);
        } else {
            int [] glyphs       = str2seq(s, A);
            int [] upper_glyphs = str2seq(s.toUpperCase(), A);
            a = new Alignment(glyphs, upper_glyphs);
        }
    }

    public static void printCharFreq() {
        System.err.println(charfreq.size() + " distinct characters");
        for(char c : charfreq.keys()) {
            System.err.println(c + " : " + charfreq.get(c));
        }
    }

    public static int [] str2seq(String x, Alphabet A) {
		int [] res = new int[x.length()];
		for(int i=0;i<x.length();i++) {
            Character c = x.charAt(i);
            if(charfreq.containsKey(c)) {
                int freq = charfreq.get(c);
                charfreq.put(c, freq+1);
            } else {
                charfreq.put(c, 1);
            }
			int index = A.lookupIndex(c);
			if(index<0)
                System.err.println("OOV character: " + x.charAt(i));
			res[i] = index;
		}
		return res;
	}

    public static int [] str2seq(String x) {
		int [] res = new int[x.length()];
		for(int i=0;i<x.length();i++) {
            Character c = x.charAt(i);
            if(charfreq.containsKey(c)) {
                int freq = charfreq.get(c);
                charfreq.put(c, freq+1);
            } else {
                charfreq.put(c, 1);
            }
			int index = A.lookupIndex(c);
			if(index<0)
                System.err.println("OOV character: " + x.charAt(i));
			res[i] = index;
		}
		return res;
	}

    public static String seq2str(int [] x) {
        StringBuilder builder = new StringBuilder();
        for(int i=0; i<x.length; i++) {
            builder.append((Character)A.lookupObject(x[i]));
        }
        return builder.toString();
    }

    public static String seq2str(int [] x, Alphabet A) {
        StringBuilder builder = new StringBuilder();
        for(int i=0; i<x.length; i++) {
            builder.append((Character)A.lookupObject(x[i]));
        }
        return builder.toString();
    }

    public int len() { return a.len(); }

    public String str() { return s; }
    public int [] glyphs() { return a.glyphs(); }
    public int [] upper_glyphs() { return a.upper_glyphs(); }
    public int [] phones() { return a.phones(); }
    public int [] pclasses() { return a.pclasses(); }

    public Character charAt(int pos) { return s.charAt(pos); }
    public int glyphAt(int pos) { return a.ch(pos); }
    public int upper_glyph(int pos) { return a.upper_ch(pos); }
    public int phoneAt(int pos) { return a.phone(pos); }
    public int classAt(int pos) { return a.pclass(pos); }

    public EntityType getType() { return type; }
    public Language getLanguage() { return language; }
    
    public static Alphabet getAlphabet() { return A; }
    public static PhoneticDictionary getPhoneticDictionary() { return P; }
    public static void setAlphabet(Alphabet A) { AnnotatedString.A = A; AnnotatedString.P = new ArpabetPhoneticDictionary(A); }
    public static void loadPhoneticDictionary(String path) { P.loadDictionary(path); }
}
