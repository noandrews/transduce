package edu.jhu.hlt.phylo.match;

import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Collection;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;

import java.text.DecimalFormat;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;

import edu.jhu.hlt.phylo.distrib.StringEditModel;
import edu.jhu.hlt.phylo.distrib.RandomNumberGenerator;
import edu.jhu.hlt.phylo.distrib.BackoffConditionalEditModel;

import edu.jhu.hlt.phylo.lang.Alphabet;

import edu.jhu.hlt.phylo.lang.AnnotatedString;
import edu.jhu.hlt.phylo.lang.PhoneticDictionary;
import edu.jhu.hlt.phylo.lang.ArpabetPhoneticDictionary;

import edu.jhu.hlt.phylo.io.WikipediaAliasReader;

public class StringEditModelTrainer {

    public static boolean USE_ALL_ALIASES = false;

    // Currently not used
    public static String normalize(String s) {
        String ret = new String(s);

        // Upper-case everything
        ret = ret.toUpperCase();

        // Non-alpha numeric characters only
        ret = ret.replaceAll("[^A-Z0-9 ]", "");
        
        return ret;
    }

    private void dumpTokens(String wiki_path, String out_path) {
        WikipediaAliasReader reader = new WikipediaAliasReader();
        reader.filter_and_parse(wiki_path);
        Set<String> clean_upper_tokens = new HashSet<String>();
        for(String name : reader.unique_names()) {
            for(String token : name.split("\\s+")) {
                String no_punct = token.replaceAll("\\P{L}+", "");
                String upper = no_punct.toUpperCase();
                if(upper.length() > 2) {
                    clean_upper_tokens.add(upper);
                }
            }
        }
        System.err.println("Writing " + clean_upper_tokens.size() + " tokens to " + out_path);
        try {
            FileWriter fw = new FileWriter(new File(out_path), true);
            BufferedWriter bw = new BufferedWriter(fw);
            for(String s : clean_upper_tokens) {
                bw.write(s+"\n");
            }
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    private void writeModel(StringEditModel transducer, String filename) {
        try {
            FileOutputStream out = new FileOutputStream(filename);
            ObjectOutputStream objOut = new ObjectOutputStream(out);
            objOut.writeObject(transducer);
            objOut.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            throw new RuntimeException();
        }
    }

    private StringEditModel readModel(String filename) {
        System.err.println("Reading model from: " + filename);
        try {
            ObjectInputStream objIn = new ObjectInputStream(new FileInputStream(filename));
            return (StringEditModel) objIn.readObject();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            throw new RuntimeException();
        }  catch(ClassNotFoundException c) {
            System.out.println(c.getMessage());
            c.printStackTrace();
            throw new RuntimeException();
        }
    }

    public static AnnotatedString [] fromList(List<AnnotatedString> list) {
        AnnotatedString [] arr = new AnnotatedString[list.size()];
        for(int i=0; i<list.size(); i++) {
            arr[i] = list.get(i);
        }
        return arr;
    }

    public static Collection<String> aliasAtRandom(List<String> aliases) {
        int index = RandomNumberGenerator.nextInt(aliases.size());
        Collection<String> ret = new ArrayList<String>();
        ret.add(aliases.get(index));
        return ret;
    }

    public static AnnotatedString [][] getPairs(String path, boolean flip, boolean use_all_aliases) {
        WikipediaAliasReader reader = new WikipediaAliasReader();
        reader.filter_and_parse(path);
        List<AnnotatedString> inputs  = new ArrayList<AnnotatedString>();
        List<AnnotatedString> outputs = new ArrayList<AnnotatedString>();
        for(String input : reader.canonical_names()) {
            Collection<String> aliases;
            if(!use_all_aliases) {
                aliases = aliasAtRandom(reader.aliases(input));
            } else {
                aliases = reader.aliases(input);
            }
            for(String output : aliases) {
                //                System.err.println("input: " + input + ", output: " + output);
                if(flip) {
                    inputs.add(new AnnotatedString(input));
                    outputs.add(new AnnotatedString(output));
                } else {
                    inputs.add(new AnnotatedString(output));
                    outputs.add(new AnnotatedString(input));
                }
            }
        }
        return new AnnotatedString [][] { fromList(inputs), fromList(outputs) };
    }

    public boolean execute(CommandLine cmd) {

        AnnotatedString [][] train = null;
        AnnotatedString [][] dev   = null;

        Alphabet A = null;
        StringEditModel model = null;

        if(cmd.hasOption("train") && cmd.hasOption("dump")) {
            dumpTokens(cmd.getOptionValue("train"), cmd.getOptionValue("dump"));
			throw new RuntimeException();
        }

        if(cmd.hasOption("input")) {
            model = readModel(cmd.getOptionValue("input"));
            A = model.getAlphabet();
            AnnotatedString.setAlphabet(A);
        }

        // ===== Training =====

        if(cmd.hasOption("train") && !cmd.hasOption("model")) {
            System.err.println("Must specify model type.");
            throw new RuntimeException();
        }

        // This will either populate a new dictionary, or use an
        // existing dictionary from a previously trained model.
        if(cmd.hasOption("train")) {
            System.err.println("Loading training pairs...");
            train = getPairs(cmd.getOptionValue("train"), cmd.hasOption("flip"), USE_ALL_ALIASES);
        }

        if(cmd.hasOption("dev")) {
            System.err.println("Loading dev pairs...");
            dev = getPairs(cmd.getOptionValue("dev"), cmd.hasOption("flip"), USE_ALL_ALIASES);
        }

        if(!cmd.hasOption("input"))
            A = AnnotatedString.getAlphabet();
        
        if(cmd.hasOption("model")) {
            String type = cmd.getOptionValue("model");
            if(type.equals("backoff")) {
                BackoffConditionalEditModel backoff = new BackoffConditionalEditModel(A.size(), A.size());
                backoff.setAlphabet(A);
                model = backoff;
            }
            else {
                System.err.println("Unknown model type: " + type);
                throw new RuntimeException();
            }
            // Train the model
            model.train(train[0], train[1], dev[0], dev[1]);
        }

        // Stop the alphabet growth
        A.stopGrowth();

        // ===== Serialize the model =====
        if(cmd.hasOption("output")) {
            writeModel(model, cmd.getOptionValue("output"));
        }
        
        // ===== Test =====
        if(cmd.hasOption("ranking")) {
            WikipediaAliasReader reader = new WikipediaAliasReader();
            reader.filter_and_parse(cmd.getOptionValue("ranking"));
            compute_mrr_title_to_alias(model, reader);
        }

        return true;
    }

    private static Options createOptions() {
		Options options = new Options();

        options.addOption("n","normalize",false,"Normalize input strings (strip punctuation, case normalize, sort)");
        options.addOption("t","train",true,"Path to training data.");
        options.addOption("d","dev",true,"Path to development data.");
        options.addOption("f","flip",false,"Flip inputs and outputs when creating training and dev pairs.");
        options.addOption("r","ranking",true,"Path to data for ranking evaluation.");
        options.addOption("m","model",true,"Model type.");
        options.addOption("i","input",true,"Path to serialized model.");
        options.addOption("o","output",true,"Path to save serialized model.");
        options.addOption("u","dump",true,"Path to dump all name tokens (to create a phonetic dictionary).");
        options.addOption("p","threads",true,"Number of threads to use.");
        options.addOption("e","everything",false,"If all the aliases are used for each entity (or just one at random).");
        options.addOption("s","sample",false,"Run some sampling tests.");

        return options;
    }

    public static void main(String [] args) {
        
        final String usage = "java " + StringEditModelTrainer.class.getName() + " [OPTIONS]";
		final CommandLineParser parser = new PosixParser();
		final Options options = createOptions();
		CommandLine cmd = null;
		final HelpFormatter formatter = new HelpFormatter();
		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e1) {
            System.err.println(e1.getMessage());
			formatter.printHelp(usage, options, true);
			throw new RuntimeException();
		}

		final StringEditModelTrainer trainer = new StringEditModelTrainer();
		final boolean success = trainer.execute(cmd);
		if(! success) {
			formatter.printHelp(usage, options, true);
			throw new RuntimeException();
		}
	}

    static private double mean_reciprocal_rank(int [] rank) {
        double sum = 0.0;
        for(int i=0;i<rank.length;i++) {
            sum += 1.0 / rank[i];
        }
        return sum / rank.length;
    }
    
    // Return the rank of the first label to match the query label
    static private int first_match_rank(List<Integer> ranked_labels, int query_label) {
        int rank;
        for(rank=0;rank<ranked_labels.size();rank++) {
            if(ranked_labels.get(rank) == query_label)
                break;
        }
        return rank+1;
    }

    static private List<Integer> rank_labels(double [] scores, List<Integer> labels) {
        List<ScoredLabel> scored_labels = new ArrayList<ScoredLabel>();
        assert(scores.length == labels.size()) : "scores length = " + scores.length + " != label length " + labels.size();
        for(int i=0;i<scores.length;i++) {
            scored_labels.add(new ScoredLabel(scores[i],labels.get(i)));
        }
        Collections.sort(scored_labels);
        List<Integer> ranked_labels = new ArrayList<Integer>();
        for(ScoredLabel l : scored_labels) {
            ranked_labels.add(l.label);
        }
        Collections.reverse(ranked_labels);
        return ranked_labels;
    }

    static class ScoredLabel implements Comparable<ScoredLabel> {
        double score;    // higher is better
        int label;
        ScoredLabel(double score, int label) {
            this.score = score;
            this.label = label;
        };
        public int compareTo(ScoredLabel other) {
            return Double.compare(score, other.score);
        }
    }

    private double compute_mrr_title_to_alias(StringEditModel em, WikipediaAliasReader reader) {
        
        List<AnnotatedString> test_names  = new ArrayList<AnnotatedString>();
        List<AnnotatedString> query_names = new ArrayList<AnnotatedString>();

        for(String name : reader.entity_canonical_names()) {
            if(reader.aliases(name).size() >= 2) {
                query_names.add(new AnnotatedString(name));
            }
        }

        for(AnnotatedString title : query_names) {

            Collection<String> aliases;
            if(!USE_ALL_ALIASES) {
                aliases = aliasAtRandom(reader.aliases(title.str()));
            } else {
                aliases = reader.aliases(title.str());
            }

            for(String variant : aliases) {
                test_names.add(new AnnotatedString(variant));
            }
        }

        // Invert the test and query sets to simulate KB linking
        List<AnnotatedString> temp = query_names;
        query_names = test_names;
        test_names  = temp;

        System.err.println(query_names.size() + " query names.");
        System.err.println(test_names.size() + " test names.");

        // Convert to ints
        int [][] qs = new int[query_names.size()][];
        for(int i=0; i<query_names.size(); i++) {
            AnnotatedString query_name = query_names.get(i);
            qs[i] = query_name.glyphs();
        }

        int [][] ts = new int[test_names.size()][];
        for(int i=0; i<test_names.size(); i++) {
            AnnotatedString test_name = test_names.get(i);
            ts[i] = test_name.glyphs();
        }

        int total_num_rankings = query_names.size();
        int num_things_to_rank = test_names.size();

        System.err.println("Producing " + total_num_rankings + " rankings of " + num_things_to_rank + " names");

        int [] gen_rank = new int[total_num_rankings];
        double [] gen_scores = new double[num_things_to_rank];

        int [] rev_gen_rank = new int[total_num_rankings];
        double [] rev_gen_scores = new double[num_things_to_rank];

        int [] max_rank = new int[total_num_rankings];
        double [] max_scores = new double[num_things_to_rank];

        int [] lev_rank = new int[total_num_rankings];
        double [] lev_scores = new double[num_things_to_rank];

        int [] jar_rank = new int[total_num_rankings];
        double [] jar_scores = new double[num_things_to_rank];

        int [] jarwink_rank = new int[total_num_rankings];
        double [] jarwink_scores = new double[num_things_to_rank];

        int [] random_rank = new int[total_num_rankings];
        double [] random_scores = new double[num_things_to_rank];

        long startTime, currTime, elapsedTime;

        AnnotatedString [] test_names_array  = fromList(test_names);
        AnnotatedString [] query_names_array = fromList(query_names);

        for(int q=0; q<total_num_rankings; q++) {

            //            System.err.println("q="+q);

            startTime = System.nanoTime();

            AnnotatedString query_name = query_names.get(q);
            int e = reader.get_name_label(query_name.str());
            List<Integer> test_labels = new ArrayList<Integer>();

            AnnotatedString [] xs = new AnnotatedString[test_names.size()];
            
            for(int t=0;t<ts.length;t++) {
                xs[t] = query_name;
            }

            // These computations are parallelized
            double [] fwd_probs = em.logp(xs, test_names_array); // xs: the query names
            double [] bwd_probs = em.logp(test_names_array, xs);

            int t = 0;
            for(AnnotatedString test_name : test_names) {
                
                if(query_name.equals(test_name)) 
                    continue;

                test_labels.add(reader.get_name_label(test_name.str()));

                double fwd = fwd_probs[t];
                double bwd = bwd_probs[t];

                // Transducer scores
                gen_scores[t] = fwd;
                rev_gen_scores[t] = bwd;
                max_scores[t] = Math.max(fwd, bwd);
                random_scores[t] = RandomNumberGenerator.nextDouble();

                t += 1;
            }

            currTime = System.nanoTime();
            elapsedTime = currTime - startTime;
            double seconds = (double)elapsedTime / 1000000000.0;
            DecimalFormat df = new DecimalFormat("#.##");
            //            System.err.println("Scored " + test_names.size() + " in " + df.format(seconds) + " sec");

            // Get a ranking & compute average precision
            List<Integer> ranked_labels = rank_labels(gen_scores, test_labels);
            gen_rank[q] = first_match_rank(ranked_labels, e);

            ranked_labels = rank_labels(rev_gen_scores, test_labels);
            rev_gen_rank[q] = first_match_rank(ranked_labels, e);

            ranked_labels = rank_labels(max_scores, test_labels);
            max_rank[q] = first_match_rank(ranked_labels, e);

            ranked_labels = rank_labels(lev_scores, test_labels);
            lev_rank[q] = first_match_rank(ranked_labels, e);

            ranked_labels = rank_labels(jar_scores, test_labels);
            jar_rank[q] = first_match_rank(ranked_labels, e);

            ranked_labels = rank_labels(jarwink_scores, test_labels);
            jarwink_rank[q] = first_match_rank(ranked_labels, e);

            ranked_labels = rank_labels(random_scores, test_labels);
            random_rank[q] = first_match_rank(ranked_labels, e);
        }

        // Compute mean average precision
        double MRR = mean_reciprocal_rank(gen_rank);
        System.err.println("Transducer MRR (gen score) = " + MRR);

        MRR = mean_reciprocal_rank(rev_gen_rank);
        System.err.println("Transducer MRR (rev gen score) = " + MRR);

        MRR = mean_reciprocal_rank(max_rank);
        System.err.println("Transducer MRR (max trans score) = " + MRR);

        MRR = mean_reciprocal_rank(lev_rank);
        System.err.println("Lev MRR = " + MRR);

        MRR = mean_reciprocal_rank(jar_rank);
        System.err.println("Jar MRR = " + MRR);

        MRR = mean_reciprocal_rank(jarwink_rank);
        System.err.println("Jaro-Winkler MRR = " + MRR);
        
        MRR = mean_reciprocal_rank(random_rank);
        System.err.println("Random MRR = " + MRR);

        return MRR;
    }
}
