package edu.jhu.hlt.phylo.util;
import java.util.AbstractList;

/** An unmodifiable Pair class, which also can be used as a {@link java.util.List}<Object> of length 2. */

public class Pair<X,Y> extends AbstractList {    

	private final X x;
	private final Y y;
	
	public Pair(X x, Y y) {
		this.x = x;
		this.y = y;
	}
	
	/** Gets the first component, with the proper type X. */
	public X get1() {    
		return x;
	}

	/** Gets the second component, with the proper type Y. */
	public Y get2() {    // returns element with the correct type; note that 
		return y;
	}

	/** Always returns 2.  Needed to implement the {@link List}<Object> interface. */
	public final int size() {  
	  return 2;
	}
	  
	/** 
	 * Needed to implement the {@link List}<Object> interface.  Should not in 
	 * general be used otherwise, since it loses information about the return
	 * type.  Note that confusingly, <code>get(0)</code> is equivalent to {@link #get1} 
	 * and <code>get(1)</code> is equivalent to {@link #get2}.
	 */ 
	public final Object get(int i) throws IndexOutOfBoundsException {
	  if (i==0) return get1();
	  else if (i==1) return get2();
	  else throw new IndexOutOfBoundsException("A pair only has elements at indices 0 and 1, not "+i);
	}
	
	/** Deep equality: that is, two Pairs are equal if they are componentwise equal */
	@Override
	public boolean equals(Object o) { 
		if (o instanceof Pair<?,?>) {
			Pair<?,?> p = (Pair<?,?>)o;
			if (Utilities.safeEquals(x, p.get1()) &&
					Utilities.safeEquals(y, p.get2())) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		int result = 17;
		result = 37*result + (x == null ? 0 : x.hashCode());
		result = 37*result + (y == null ? 0 : y.hashCode());
		return result;
	}

	/** Returns a value of the form "(x,y)". */
        @Override
        public String toString()
        { 
                return "(" + x + "," + y + ")"; 
        }
}
