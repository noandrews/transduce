package edu.jhu.hlt.phylo.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import cc.mallet.types.Alphabet;

/**
 * Various convenience functions.
 * 
 * @author Matt Gormley
 *
 */
public class Utilities {
	
	private Utilities() {
		// private constructor
	}
	
	public static boolean safeEquals(Object o1, Object o2) {
		if (o1 == null || o2 == null) {
			return o1 == o2;
		} else {
			return o1.equals(o2);
		}
	}
	
	public static <X> void increment(Map<X,Integer> map, X key, Integer incr) {
		if (map.containsKey(key)) {
			Integer value = map.get(key);
			map.put(key, value + incr);
		} else {
			map.put(key, incr);
		}
	}
	
	public static <X> void increment(Map<X,Double> map, X key, Double incr) {
		if (map.containsKey(key)) {
			Double value = map.get(key);
			map.put(key, value + incr);
		} else {
			map.put(key, incr);
		}
	}

	/**
	 * @return The resulting set
	 */
	public static <K,V> Set<V> addToSet(Map<K,Set<V>> map, K key, V value) {
	    Set<V> values;
	    if (map.containsKey(key)) {
            values = map.get(key);
            values.add(value);
        } else {
            values = new HashSet<V>();
            values.add(value);
            map.put(key, values);
        }
	    return values;
    }
	
	/**
	 * Choose the <X> with the greatest number of votes.
	 * @param <X> The type of the thing being voted for.
	 * @param votes Maps <X> to a Double representing the number of votes
	 * 				it received.
	 * @return The <X> that received the most votes.
	 */
	public static <X> List<X> mostVotedFor(Map<X, Double> votes) {
		// Choose the label with the greatest number of votes
		// If there is a tie, choose the one with the least index
		double maxTalley = Double.NEGATIVE_INFINITY;
		List<X> maxTickets = new ArrayList<X>();
		for(Entry<X,Double> entry : votes.entrySet()) {
			X ticket = entry.getKey();
			double talley = entry.getValue();
			if (talley > maxTalley) {
				maxTickets = new ArrayList<X>();
				maxTickets.add(ticket);
				maxTalley = talley;
			} else if (talley == maxTalley) {
				maxTickets.add(ticket);
			}
		}
		return maxTickets;
	}

	/**
	 * Choose the Label which has the least value for its index.
	 */
	// public static Label breakTie(List<Label> mostVotes) {
	// 	if (mostVotes.size() == 0) {
	// 		throw new RuntimeException("Cannot break a tie between 0 votes");
	// 	}
		
	// 	ClassificationLabel minIndexLabel = new ClassificationLabel(Integer.MAX_VALUE);
	// 	for(Label l : mostVotes) {
	// 		ClassificationLabel label = (ClassificationLabel)l;
	// 		if (label.value() < minIndexLabel.value()) {
	// 			minIndexLabel = label;
	// 		}
	// 	}
	// 	return minIndexLabel;
	// }

    public static int [] intify(String x, Alphabet a) {
        boolean grow_alphabet = false;
        int [] res = new int[x.length()];
		for(int i=0;i<x.length();i++) {
			int index = a.lookupIndex(x.charAt(i), grow_alphabet);
			if(index<0) {
                //                oov_types.add(x.charAt(i));
                //                num_oov_tokens ++;
                index = 0;
			}
			res[i] = index;
		}
		return res;
    }
    
}
